/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Dominio;

import Comunicacion.ManejadorXML;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author isaacarismendi
 */
public class Transporte implements Serializable{
    public static Transporte transporte;
    private String informacionControl;
    private List<Paquete> listaPaquetes;
    private int tiempoTrayectoria;
    private ManejadorXML xml;

    private Transporte() {
        this.informacionControl = xml.getInstanceXML().leerXml();
        this.listaPaquetes = new ArrayList<>();
        this.tiempoTrayectoria = 4; // Tiempo que tarda por defecto
    }
    
    public static Transporte getInstanceTransporte (Transporte transporteRecibido){
        transporte=transporteRecibido;
        return transporte;
    }
    
    public static Transporte getInstanceTransporte (){
        if (transporte==null) {
            transporte=new Transporte();
        }
        return transporte;
    }
    
    public String getInformacionControl() {
        return informacionControl;
    }

    public void setInformacionControl(String informacionControl) {
        this.informacionControl = informacionControl;
    }

    public List<Paquete> getListaPaquetes() {
        return listaPaquetes;
    }
    
    public int getTiempoTrayectoria() {
        return tiempoTrayectoria;
    }

    public void setTiempoTrayectoria(int tiempoTrayectora) {
        this.tiempoTrayectoria = tiempoTrayectora;
    }

    
//    public void setListaPaqutes(List<Paquete> listaPaqutes) {
//        this.listaPaqutes = listaPaqutes;
//    }
    public void almacenarInformacionSucursal (Paquete paquete, String detalles) {
        
        xml.getInstanceXML().agregarInformacionControl(Integer.toString(paquete.getId()),paquete.getIpSucursalOrigen(),paquete.getIpSucursalDestino(),detalles);
        
    }
    
    public void recalcularTiempoTrayectoria() {
        
        if (listaPaquetes.size()>6) {
            int tiempoAdicional= listaPaquetes.size()-6;
            tiempoTrayectoria=tiempoTrayectoria+tiempoAdicional;
        }
        else if (listaPaquetes.size()<=6) {
            tiempoTrayectoria=4;
        }
    }
    
    public Boolean addItemListaPaquetes(Paquete paquete) {    
        int tiempoDeRecepcion = 1;
        if(this.listaPaquetes.size() < 11){
            paquete.getEstadisticas().setTiempoLlegadaDestino(tiempoDeRecepcion);
            this.listaPaquetes.add(paquete);
            almacenarInformacionSucursal(paquete, "Paquete Enviado Exitosamente");            
            recalcularTiempoTrayectoria();
            return true;
        }
        paquete.getEstadisticas().setNoAceptadoTranspote(paquete.getEstadisticas().getNoAceptadoTranspote()+1);
        return false;
    }
    
    public void setearTiempoProcesamientoInformacionControl () {
        for (Paquete paquete : listaPaquetes) {
            paquete.getEstadisticas().setTiempoLlegadaDestino(paquete.getEstadisticas().getTiempoLlegadaDestino()+2);
            
        }
    }
    
    public void setearTiempoTrayecto () {
        
        for (Paquete paquete : listaPaquetes) {
            paquete.getEstadisticas().setTiempoLlegadaDestino(paquete.getEstadisticas().getTiempoLlegadaDestino()+tiempoTrayectoria);
            
    
        }
    }
    
}

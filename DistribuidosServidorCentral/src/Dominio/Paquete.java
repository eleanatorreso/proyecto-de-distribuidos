/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Dominio;

import java.io.Serializable;

/**
 *
 * @author isaacarismendi
 */
public class Paquete implements Serializable{
   
    private String data;
    private Bitacora estadisticas;
    private String ipSucursalDestino;
    private String ipSucursalOrigen;
    private int id;
    private static int idNumber=0;
    private int tiempoEsperaEnRecepcion;
    
    public Paquete(String data, String ipSucursalDestino) {
        
        this.id = idNumber++;
        this.data = data;
        this.ipSucursalDestino = ipSucursalDestino;
        this.tiempoEsperaEnRecepcion = 0;
        this.ipSucursalOrigen = Sucursal.getInstanceSucursal().getIp();
        this.estadisticas = new Bitacora(this.ipSucursalDestino, this.ipSucursalOrigen);
    }

    public void setTiempoEsperaEnRecepcion(int tiempoEsperaEnRecepcion) {
        this.tiempoEsperaEnRecepcion = tiempoEsperaEnRecepcion;
    }

    public int getTiempoEsperaEnRecepcion() {
        return tiempoEsperaEnRecepcion;
    }

    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getIpSucursalDestino() {
        return ipSucursalDestino;
    }

    public void setIpSucursalDestino(String ipSucursalDestino) {
        this.ipSucursalDestino = ipSucursalDestino;
    }

    public String getIpSucursalOrigen() {
        return ipSucursalOrigen;
    }

    public void setIpSucursalOrigen(String ipSucursalOrigen) {
        this.ipSucursalOrigen = ipSucursalOrigen;
    }
    
    public String getIp() {
        return data;
    }

    public void setIp(String data) {
        this.data = data;
    }

    public Bitacora getEstadisticas() {
        return estadisticas;
    }

    public void setEstadisticas(Bitacora estadisticas) {
        this.estadisticas = estadisticas;
    }
    
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Dominio;

import java.io.Serializable;

/**
 *
 * @author isaacarismendi
 */
public class Bitacora implements Serializable{
    
    private int tiempoLlegadaDestino; //tiempo total
    private int tiempoEspera; //tiempo de espera para ser recibido
    private int noAceptadoSucursal;
    private int noAceptadoTranspote;
    private String ipSucursalDestino;
    private String ipSucursalOrigen;
    private int noAceptadoListaEsperaEnvio; //no fue aceptado para enviar y se perdio el paquete
    private int noAceptadoListaEsperaRecepcion; //no fue aceptado para recepcion en la sucursal
    private int sucursalCaida = 0;
    private int desaparicionDelTransporte = 0;
    private int mensajesPerdidosAlServidor  = 0;
    

    public Bitacora(String ipSucursalDestino, String ipSucursalOrigen) {
        this.tiempoLlegadaDestino = 0;
        this.tiempoEspera = 0;
        this.noAceptadoSucursal = 0;
        this.noAceptadoTranspote = 0;
        this.noAceptadoListaEsperaEnvio = 0;
        this.noAceptadoListaEsperaRecepcion = 0;
        this.ipSucursalDestino = ipSucursalDestino;
        this.ipSucursalOrigen = ipSucursalOrigen;
    }

    public int getSucursalCaida() {
        return sucursalCaida;
    }

    public void setSucursalCaida(int sucursalCaida) {
        this.sucursalCaida = sucursalCaida;
    }

    public int getDesaparicionDelTransporte() {
        return desaparicionDelTransporte;
    }

    public void setDesaparicionDelTransporte(int desaparicionDelTransporte) {
        this.desaparicionDelTransporte = desaparicionDelTransporte;
    }

    public int getMensajesPerdidosAlServidor() {
        return mensajesPerdidosAlServidor;
    }

    public void setMensajesPerdidosAlServidor(int mensajesPerdidosAlServidor) {
        this.mensajesPerdidosAlServidor = mensajesPerdidosAlServidor;
    }
    
    public int getNoAceptadoListaEsperaEnvio() {
        return noAceptadoListaEsperaEnvio;
    }

    public void setNoAceptadoListaEsperaEnvio(int noAceptadoListaEsperaEnvio) {
        this.noAceptadoListaEsperaEnvio = noAceptadoListaEsperaEnvio;
    }

    public int getNoAceptadoListaEsperaRecepcion() {
        return noAceptadoListaEsperaRecepcion;
    }

    public void setNoAceptadoListaEsperaRecepcion(int noAceptadoListaEsperaRecepcion) {
        this.noAceptadoListaEsperaRecepcion = noAceptadoListaEsperaRecepcion;
    }
    
    public int getTiempoLlegadaDestino() {
        return tiempoLlegadaDestino;
    }

    public void setTiempoLlegadaDestino(int tiempoLlegadaDestino) {
        this.tiempoLlegadaDestino = tiempoLlegadaDestino;
    }

    public int getTiempoEspera() {
        return tiempoEspera;
    }

    public void setTiempoEspera(int tiempoEspera) {
        this.tiempoEspera = tiempoEspera;
    }

    public int getNoAceptadoSucursal() {
        return noAceptadoSucursal;
    }

    public void setNoAceptadoSucursal(int noAceptadoSucursal) {
        this.noAceptadoSucursal = noAceptadoSucursal;
    }

    public int getNoAceptadoTranspote() {
        return noAceptadoTranspote;
    }

    public void setNoAceptadoTranspote(int noAceptadoTranspote) {
        this.noAceptadoTranspote = noAceptadoTranspote;
    }

    public String getIpSucursalDestino() {
        return ipSucursalDestino;
    }

    public void setIpSucursalDestino(String ipSucursalDestino) {
        this.ipSucursalDestino = ipSucursalDestino;
    }

    public String getIpSucursalOrigen() {
        return ipSucursalOrigen;
    }

    public void setIpSucursalOrigen(String ipSucursalOrigen) {
        this.ipSucursalOrigen = ipSucursalOrigen;
    }    
    
}

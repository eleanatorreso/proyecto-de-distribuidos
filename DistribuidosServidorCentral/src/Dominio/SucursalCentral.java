/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Dominio;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author isaacarismendi
 */
public class SucursalCentral {
    
    private List<String> listaSucursales;
    private List<Bitacora> listaEstadistica;
    private String ip;

    public SucursalCentral() {
        this.listaSucursales = new ArrayList<>();
        this.listaEstadistica = new ArrayList<>();
        try {
            InetAddress myIp = InetAddress.getLocalHost();
            this.ip = myIp.getHostAddress();
        } catch (UnknownHostException ex) {
            this.ip = null;
        }
        
    }

    public String getIp() {
        return ip;
    }
    
    public void setListaEstadistica(List<Bitacora> listBitacora){
        this.listaEstadistica = listBitacora;
    }

    public List<String> getListaSucursales() {
        return listaSucursales;
    }

    public void setListaSucursales(List<String> listaSucursales) {
        this.listaSucursales = listaSucursales;
    }
    
    public Boolean addListaSucursales(String ip){
        Boolean resultado = true;
        
        for (String elemento : listaSucursales) {
            
            if(elemento.equals(ip)){
                resultado = false;
                break;
            }
            else{
                resultado = true;  
            }
            
        }
        
        if(resultado){
            resultado = this.listaSucursales.add(ip);
        }
                
        return resultado;
    }

    public List<Bitacora> getListaEstadistica() {
        return listaEstadistica;
    }

//    public void setListaEstadistica(List<Bitacora> listaEstadistica) {
//        this.listaEstadistica = listaEstadistica;
//    }
    
    public Boolean addItemListaEstadistica(Bitacora bitacora){
        Boolean resultado = false;
        resultado = this.listaEstadistica.add(bitacora);
        return resultado;
    }
    
}

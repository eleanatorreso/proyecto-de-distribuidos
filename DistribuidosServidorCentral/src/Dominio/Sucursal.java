/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Dominio;

import Comunicacion.ManejadorXML;
import java.io.Serializable;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author isaacarismendi
 */
public class Sucursal implements Serializable{
    
    private static Sucursal sucursal;
    private String miIp;
    private String ipDestino;
    private List<Paquete> listaEsperaRecepcion;
    private List<Paquete> listaEsperaEnvio;
    private int tiempoEsperaEnLista;
    private ManejadorXML xml;
    private int paquetesProcesados;

//    private Sucursal(String miIp, String ipDestino,int tiempoEsperaEnLista) {
//        this.miIp = miIp;
//        this.ipDestino = ipDestino;
//        this.listaEspetaRecepcion = new ArrayList<>();
//        this.listaEspetaEnvio = new ArrayList<>();
//        this.tiempoEsperaEnLista = tiempoEsperaEnLista;
//    }
    
    private Sucursal() {
        InetAddress IP;
        try {
            IP = InetAddress.getLocalHost();
            this.miIp = IP.getHostAddress();
        } catch (UnknownHostException ex) {
            this.miIp = "";
            Logger.getLogger(Sucursal.class.getName()).log(Level.SEVERE, null, ex);
        } 
        this.listaEsperaRecepcion = new ArrayList<>();
        this.listaEsperaEnvio = new ArrayList<>();
        this.tiempoEsperaEnLista = 0;
    }
    
    
    public static Sucursal getInstanceSucursal (){
        if (sucursal==null) {
            sucursal=new Sucursal();
        }        
        return sucursal;
    }
    
    public String getIp() {
        return miIp;
    }

    public void setIp(String miIp) {
        this.miIp = miIp;
    }

    public String getIpDestino() {
        return ipDestino;
    }

    public void setIpDestino(String ipDestino) {
        this.ipDestino = ipDestino;
    }
    
    public int getTiempoEsperaEnLista() {
        return tiempoEsperaEnLista;
    }

    public void setTiempoEsperaEnLista(int tiempoEsperaEnLista) {
        this.tiempoEsperaEnLista = tiempoEsperaEnLista;
    }
    
    public List<Paquete> getListaEsperaRecepcion() {
        return listaEsperaRecepcion;
    }
    
    public List<Paquete> getListaEsperaEnvio() {
        return listaEsperaEnvio;
    }

    public int getPaquetesProcesados() {
        return paquetesProcesados;
    }

    public void setPaquetesProcesados(int paquetesProcesados) {
        this.paquetesProcesados = paquetesProcesados;
    }
    
    
//    public void setListaEspetaRecepcion(List<Paquete> listaEspetaRecepcion) {
//        this.listaEspetaRecepcion = listaEspetaRecepcion;
//    }
    
    public void almacenarInformacionSucursal (Paquete paquete, String detalles) {
        
        xml.getInstanceXML().agregarInformacionControl(Integer.toString(paquete.getId()),paquete.getIpSucursalOrigen(),paquete.getIpSucursalDestino(),detalles);   
    }
    
    public void procesarPaquetes (Paquete paquete) {
        
        Paquete paqueteBorrar = null;
        paquete.getEstadisticas().setTiempoLlegadaDestino(paquete.getEstadisticas().getTiempoLlegadaDestino()+15+this.getTiempoEsperaEnLista());
        paquete.getEstadisticas().setTiempoEspera(this.tiempoEsperaEnLista);
        paqueteBorrar = paquete;
        //enviar a central  
        
        almacenarInformacionSucursal(paquete,"Paquete Procesado Exitosamente");

        if (paqueteBorrar != null) {
            listaEsperaRecepcion.remove(paqueteBorrar);
        }
        this.paquetesProcesados++;
    }
    
    public Boolean addItemListaEsperaRecepcion(Paquete paquete){        
        
        if(this.listaEsperaRecepcion.size() <= 5){
            System.out.println("Recibi paquete de:"+paquete.getIpSucursalOrigen());
            paquete.setTiempoEsperaEnRecepcion(this.tiempoEsperaEnLista);
            this.listaEsperaRecepcion.add(paquete);
            almacenarInformacionSucursal(paquete,"Paquete Recibido Exitosamente");
            return true;
        }        
        return false;
    }

//    public void setListaEspetaEnvio(List<Paquete> listaEspetaEnvio) {
//        this.listaEspetaEnvio = listaEspetaEnvio;
//    }
    
    public Boolean addItemListaEsperaEnvio(Paquete paquete){
        
        if(this.listaEsperaEnvio.size() <= 7){
            this.listaEsperaEnvio.add(paquete);
            return true;
        }
        return false;
    }
    
}

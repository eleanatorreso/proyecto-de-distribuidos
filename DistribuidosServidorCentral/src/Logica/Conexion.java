/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Logica;

import Dominio.SucursalCentral;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.rmi.NotBoundException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author isaacarismendi
 */
public class Conexion {
    private static Registry registry = null;
    private static UnicastRemoteObject obj;
    public static SucursalCentral servidorCentral = new SucursalCentral();
    private static String ultimaSucursalEnvioTransporte = "";
    private static HiloChequearTransporte hiloChequearTransporte;
    
    private static void iniciarRegistro()  {
        if (registry == null) {
            try {
                registry = LocateRegistry.createRegistry(8881);
                obj = new Servidor();
                registry.rebind("Server", obj);
                hiloChequearTransporte = new HiloChequearTransporte();
            } catch (RemoteException ex) {
                Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public static RMIGrandulon connectServidor(String ip){
        
        try{
            
            Registry reg = LocateRegistry.getRegistry(ip, 8883); // conexion a la sucursal 
            RMIGrandulon rmiGrandulon = (RMIGrandulon) reg.lookup("ServerGrandulon");
            return rmiGrandulon;
            
        }catch(Exception e){  
            return null;
        }      
    }
    
    public static RMISucursal connectSucursal(String ip){
        try{
            
            Registry reg = LocateRegistry.getRegistry(ip, 8882); // conexion a la sucursal 
            RMISucursal rmiSucursal = (RMISucursal) reg.lookup("Sucursal");
            return rmiSucursal;
            
        }catch(Exception e){
            return null;
        }      
    }
    
    public static void starServerGrandulon(){
        try{
            InetAddress ip = InetAddress.getLocalHost();
            System.setProperty("java.rmi.server.hostname", ip.getHostAddress());
            Registry reg = LocateRegistry.createRegistry(8883);
            reg.rebind("ServerGrandulon", (Remote) new ServerGrandulon());
            System.out.println("Servidor Encendido puerto 8883");
        }   catch(Exception e){

        }  
    }
    
    public static void starServer(){
        try {        
            List<String> listServer = broadcastServer();
           
            String ip = InetAddress.getLocalHost().getHostAddress();
            String ipMayor = listServer.get(listServer.size()-1);
            if(ipMayor.equals(ip))
            {
                System.out.println("Servidor Encendido 8881");
                System.setProperty("java.rmi.server.hostname", ip);
                iniciarRegistro();
            }
            else
            {
                stopServer();
            }
        } catch (UnknownHostException ex) {
            Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
        }catch(ArrayIndexOutOfBoundsException ex2){
            System.out.println("NO tiene internet");

        }
    }
    
    public static void stopServer(){
        try {
            registry.unbind("Server");
            System.out.println("Detenido puerto 8881");
            //LocateRegistry.getRegistry(8881).unbind("Server");
            UnicastRemoteObject.unexportObject(obj, true);
            System.gc();
            replicarEstadisticas();
            replicarListaSucursal();
            hiloChequearTransporte.hilo.stop();
        } catch (NullPointerException | RemoteException | NotBoundException ex) {
            
        }
    }
    
    public static List<String> broadcastServer(){
        List<String> listServer = new ArrayList<>();
        listServer.add("192.168.1.102");
        listServer.add("192.168.1.100");
        listServer.add("192.168.1.103");
        
        List<String> listServerConectados = new ArrayList<>();
        for (String ip : listServer) {
            if(connectServidor(ip) != null){
                listServerConectados.add(ip);
            }
        }
        Collections.sort(listServerConectados, new Comparator<String>() {
        @Override
        public int compare(String o1, String o2) {
            String[] ips1 = o1.split("\\.");
            String updatedIp1 = String.format("%3s.%3s.%3s.%3s",
                                                  ips1[0],ips1[1],ips1[2],ips1[3]);
            String[] ips2 = o2.split("\\.");
            String updatedIp2 = String.format("%3s.%3s.%3s.%3s",
                                                  ips2[0],ips2[1],ips2[2],ips2[3]);
            return updatedIp1.compareTo(updatedIp2);
        }
    });
        //ordenara de menor a mayor
        return listServerConectados;
    }
    
    public static void replicarEstadisticas(){
        List<String> listServer = new ArrayList<>();
        listServer.add("192.168.1.102");
        listServer.add("192.168.1.100");
        listServer.add("192.168.1.103");
        
        for (String ip : listServer) {
            if(!ip.equals(servidorCentral.getIp())){
                RMIGrandulon rmiGrandulon = connectServidor(ip);
                 try {
                     if(rmiGrandulon != null){
                      rmiGrandulon.enviarBitacora(servidorCentral.getListaEstadistica());   
                     }
                 } catch (Exception ex) {

                 } 
            }
        }
    }
    
     public static void replicarListaSucursal(){
        List<String> listServer = new ArrayList<>();
        listServer.add("192.168.1.102");
        listServer.add("192.168.1.100");
        listServer.add("192.168.1.103");
        
        for (String ip : listServer) {
            if(!ip.equals(servidorCentral.getIp())){
                RMIGrandulon rmiGrandulon = connectServidor(ip);
                 try {
                     if(rmiGrandulon != null){
                         rmiGrandulon.enviarListaSucursales(servidorCentral.getListaSucursales(), ultimaSucursalEnvioTransporte);   
                     }
                 } catch (Exception ex) {

                 } 
            }
        }
    }

    public static void setUltimaSucursalEnvioTransporte(String ultimaSucursalEnvioTransporte) {
        Conexion.ultimaSucursalEnvioTransporte = ultimaSucursalEnvioTransporte;
    }

    public static String getUltimaSucursalEnvioTransporte() {
        return ultimaSucursalEnvioTransporte;
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Logica;

import Dominio.Bitacora;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

/**
 *
 * @author isaacarismendi
 */
public interface RMIGrandulon extends Remote{
    
    public void enviarBitacora(List<Bitacora> listBitacora) throws RemoteException;
    public void enviarListaSucursales(List<String> listIPSucursales, String ultimaSucursalEnvioTransporte) throws RemoteException;
    
}

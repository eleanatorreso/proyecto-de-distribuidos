/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Logica;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author isaacarismendi
 */
public class HiloChequearTransporte implements Runnable{
    public Thread hilo;

    public HiloChequearTransporte() {
        hilo=new Thread(this);
        hilo.start();
    }

    
    @Override
    public void run() {
        RMISucursal rmiSucursal;
        Boolean resultado = false;
        List<String> listaAux;
        while (true) {     
            try {
                if(Conexion.servidorCentral.getListaSucursales().size() > 1){
                    listaAux = new ArrayList<>(Conexion.servidorCentral.
                                getListaSucursales());
                    listaAux.addAll(Conexion.servidorCentral.
                                getListaSucursales());
                    for (Iterator<String> it = listaAux.iterator(); it.hasNext();) {

                        String ip = it.next();
                        rmiSucursal = Conexion.connectSucursal(ip);
                        if(rmiSucursal != null){
                            resultado = rmiSucursal.tienesElTransporte();
                            if(resultado){
                                break;
                            }
                        }

                        Thread.sleep(1500);
                    }
                    

                    if(!resultado){
                        rmiSucursal = Conexion.connectSucursal(Conexion.getUltimaSucursalEnvioTransporte());
                        if (rmiSucursal != null) {
                            rmiSucursal.reenviarTransporte();
                        }
                    }
         
                }
                Thread.sleep(5000);
                
            } catch (RemoteException ex) {
                Logger.getLogger(HiloChequearTransporte.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InterruptedException ex) {
                Logger.getLogger(HiloChequearTransporte.class.getName()).log(Level.SEVERE, null, ex);
            } 
        }
        
    }
    
}

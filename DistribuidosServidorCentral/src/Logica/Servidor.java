/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Logica;

import Comunicacion.ManejadorXML;
import Dominio.Bitacora;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author isaacarismendi
 */
public class Servidor extends UnicastRemoteObject implements RMI{
    
    private double tiempoTotalCargaMaxima = 0;
    private double tiempoDeVida = 1;
    private double numeroPaquetesPerdidos = 0;
    private double numeroPaquetesNoAceptadoListaRecibidos = 0;
    
    private ManejadorXML xml = new ManejadorXML();
    
    
   public Servidor() throws RemoteException{
       super();
   }
   
    @Override
    public Boolean ConectarmeAlAnillo(String ip) throws RemoteException {
        Boolean resultado; 
        RMISucursal rmiSucursal;
        resultado = Conexion.servidorCentral.addListaSucursales(ip);
        
        if(resultado){
            enviarListaIp();
            if(Conexion.servidorCentral.getListaSucursales().size() == 1){
                rmiSucursal = Conexion.connectSucursal(Conexion.servidorCentral.getListaSucursales().get(0));
                rmiSucursal.crearTransporte();
            }
            
        }
        else{
            System.out.println("La IP " + ip + " esta repetida NO se puedo conectar en el anillo" );
        }
        return resultado;
    }
    
   
    
    public void enviarListaIp(){
        RMISucursal rmiSucursal;
        for (Iterator<String> it =  Conexion.servidorCentral.getListaSucursales().iterator(); it.hasNext();) {
            String elemento = it.next();
            rmiSucursal = Conexion.connectSucursal(elemento);
            try {
                rmiSucursal.actulizarAnillo(Conexion.servidorCentral.getListaSucursales());
            } catch (RemoteException ex) {
                Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        Conexion.replicarListaSucursal();
    }    
    
    @Override
    public Boolean recibirBitacora(Bitacora bitacora) throws RemoteException {
        Boolean resultado = false;
        resultado = Conexion.servidorCentral.addItemListaEstadistica(bitacora);
        if(resultado){
         // System.out.println("Recibida estadisticas del paquete con destino " + bitacora.getIpSucursalDestino());
        
          calcularEstadisticas();
          Conexion.replicarEstadisticas();
        }
        return resultado;
    }
    
    @Override
    public void enviarEstadoTransporte (int tiempoTransporte, int tiempoDeVida) throws RemoteException {
        this.tiempoTotalCargaMaxima += tiempoTransporte;
        this.tiempoDeVida = tiempoDeVida;
    }
    
    @Override
    public void recibirInformacionControl (String stringXxml) throws RemoteException {
        xml.agregarInformacion(stringXxml);
    }
    
    private void calcularEstadisticas(){
        System.out.println("\n\n********************************************** ESTADISTICAS"
                         + " *********************************************");
        tiempoPromedioPorSucursal();
        System.out.println("\n***********************************************************"
                         + "***********************************************");
        
        
    }
    
    private void tiempoPromedioPorSucursal(){
        
        double tiempoPromedioEnllegar = 0;
        double tiempoPromedioEnRecepcion = 0;
        double cantidadPaqueteNoAceptados = 0;
        double porcetajeNoAceptadosEnRecepcion = 0;
        double numeroPaquetesQueEntranYSalenALaPrimera = 0;
        double tiempoPromedioNoSalenListaDeEspera = 0;
        int sucursalCaida = 0;
        int desaparicionDelTransporte = 0;
        int mensajesPerdidosAlServidor  = 0;
        int contador = 0;
        List<Bitacora> listaBitacora = new ArrayList<>();
        
        for (String ip : Conexion.servidorCentral.getListaSucursales()) {
            listaBitacora = buscarBitacorasSucursal(ip);
            tiempoPromedioEnllegar = 0;
            tiempoPromedioEnRecepcion = 0;
            cantidadPaqueteNoAceptados = 0;
            porcetajeNoAceptadosEnRecepcion = 0;
            contador = 0;
            numeroPaquetesPerdidos = 0;
            numeroPaquetesNoAceptadoListaRecibidos = 0;
            for (Bitacora bitacora : listaBitacora) {
                
                numeroPaquetesPerdidos += bitacora.getNoAceptadoListaEsperaEnvio();
                numeroPaquetesNoAceptadoListaRecibidos += bitacora.getNoAceptadoTranspote();
                sucursalCaida += bitacora.getSucursalCaida();
                desaparicionDelTransporte += bitacora.getDesaparicionDelTransporte();
                mensajesPerdidosAlServidor  += bitacora.getMensajesPerdidosAlServidor();
                tiempoPromedioEnllegar += bitacora.getTiempoLlegadaDestino();
                
                tiempoPromedioEnRecepcion += bitacora.getTiempoEspera();
                
                if(bitacora.getNoAceptadoSucursal() > 0){
                    cantidadPaqueteNoAceptados +=1;
                }
                
                porcetajeNoAceptadosEnRecepcion = (cantidadPaqueteNoAceptados * 
                                                        100)/listaBitacora.size();
                
                if(bitacora.getNoAceptadoSucursal() == 0 && bitacora.getNoAceptadoTranspote() == 0){
                    numeroPaquetesQueEntranYSalenALaPrimera++;
                }
                
                if (bitacora.getNoAceptadoTranspote() > 0) {
                    tiempoPromedioNoSalenListaDeEspera +=1;
                }
                
                contador++;
            }
            
            System.out.println("\n------------ " +ip + " ------------");
            System.out.println("Tiempo promedio que tardan los paquetes de la sucursal "
                    + "en LLEGAR a su destino "+tiempoPromedioEnllegar/contador);
            System.out.println("Promedio del tiempo de espera en RECEPCIÓN de paquetes "
                    + tiempoPromedioEnRecepcion/contador+ " en la sucursal");
            System.out.println("Cantidad de paquetes NO ACEPTADOS " + cantidadPaqueteNoAceptados);
            System.out.println("Porcetajes del total de NO ACEPTADOS con respecto "
                    + "a todos los enviados " + porcetajeNoAceptadosEnRecepcion + "%");        
            System.out.println("Cantidad de paquetes perdidos en la Sucursal "+numeroPaquetesPerdidos);
            System.out.println("Cantindad de paquetes no aceptados en la Lista de Recepcion de la Sucursal "+numeroPaquetesNoAceptadoListaRecibidos);
 
        }      
        System.out.println("\n------------ GENERAL ------------");
        System.out.println("Porcentaje de paquetes que entran al primer intento "
                    + "en el transporte \ny salen al primer intento del transporte " + 
                        (numeroPaquetesQueEntranYSalenALaPrimera * 100)
                        /Conexion.servidorCentral.getListaEstadistica().size() + "%");
        System.out.println("\nConteo de veces que los paquetes no salen de la lista de " +
                            "espera de envío \npor saturación del transporte "+ 
                            tiempoPromedioNoSalenListaDeEspera/Conexion.servidorCentral.
                                    getListaEstadistica().size() +"\n");
        System.out.println("Porcentaje de tiempo en que el transporte circula con su carga "
                    + "máxima: "+(this.tiempoTotalCargaMaxima/this.tiempoDeVida) * 100 + "%");
        
        System.out.println("\n------------ ESTADISTICAS ESPECIALES ------------");
        System.out.println("Cantidad de veces que una sucursal se ha caido: "+sucursalCaida);
        System.out.println("Cantidad de mensajes perdidos al servidor: "+ mensajesPerdidosAlServidor);
        System.out.println("Cantidad de veces que el transporte ha desaparecido: "+desaparicionDelTransporte);
    }
   
    private List<Bitacora> buscarBitacorasSucursal(String ipSucursal){
        
        List<Bitacora> listaResultado = new ArrayList<>();
        for (Bitacora item : Conexion.servidorCentral.getListaEstadistica()) {
            
            if(item.getIpSucursalOrigen().equals(ipSucursal)){
                listaResultado.add(item);
            }
        }
        
        return listaResultado;
    }

    
    
    public void broadcast(){
        RMISucursal resultado;
        List<String> listaAux = new ArrayList<>(Conexion.servidorCentral.getListaSucursales()); 
        for (Iterator<String> it = listaAux.iterator(); it.hasNext();) {
            String ip = it.next();
            resultado = Conexion.connectSucursal(ip);
            if(resultado == null){
                it.remove();
            }
           
        }
        Conexion.servidorCentral.setListaSucursales(listaAux);
    }
    
    @Override
    public void reconstruirAnillo() throws RemoteException {
        broadcast();
        enviarListaIp();
    }

    @Override
    public void notificarEnvioTransporte(String ipSucursalOrigen) throws RemoteException {
        Conexion.setUltimaSucursalEnvioTransporte(ipSucursalOrigen);
    }
    
}

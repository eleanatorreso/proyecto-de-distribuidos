/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Logica;

import Dominio.Bitacora;
import Dominio.Paquete;
import Dominio.SucursalCentral;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.List;

/**
 *
 * @author isaacarismendi
 */
public class ServerGrandulon extends UnicastRemoteObject implements RMIGrandulon{

    public ServerGrandulon() throws RemoteException{
       super();
   }

    @Override
    public void enviarBitacora(List<Bitacora> listBitacora) throws RemoteException {
        Conexion.servidorCentral.setListaEstadistica(listBitacora);
    }

    @Override
    public void enviarListaSucursales(List<String> listIPSucursales, String ultimaSucursalEnvioTransporte) throws RemoteException {
        Conexion.servidorCentral.setListaSucursales(listIPSucursales);
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Logica;

import Dominio.Bitacora;
import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 *
 * @author isaacarismendi
 */
interface RMI extends Remote{
     //public String getText(String text) throws RemoteException;
    
    public Boolean ConectarmeAlAnillo(String ip) throws RemoteException;
    public Boolean recibirBitacora(Bitacora bitacora) throws RemoteException;
    public void enviarEstadoTransporte (int tiempoTransporte, int tiempoDeVida) throws RemoteException;   
    public void recibirInformacionControl (String stringXxml) throws RemoteException;
    public void reconstruirAnillo () throws RemoteException;
    public void notificarEnvioTransporte (String ipSucursalOrigen) throws RemoteException;
}

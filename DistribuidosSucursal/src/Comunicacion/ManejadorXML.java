package Comunicacion;

import java.util.List;
import java.io.IOException;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;
import java.io.File;
import java.io.FileWriter;
import java.io.Serializable;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.CharacterData;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class ManejadorXML implements Serializable{
     /* Almacenara el archivo que contiene los datos */
    private File archivo;
    /* Almacenara el documento XML */
    private Document documento;
    /* Almacenara el nodo principal del documento */
    private Element nodoRaiz;
    private static ManejadorXML manejador;

    public ManejadorXML() {
        
    } 
    
    public static ManejadorXML getInstanceXML (){
        if (manejador==null) {
            manejador=new ManejadorXML();
        }        
        return manejador;
    }
    
    public String leerXml(){
        String resultado="";
        /* Creamos una instancia del archivo data.xml */
        archivo = new File(System.getProperty("user.dir")+"//InformacionDeControl.xml");
        /* Creamos el documento xml a partir del archivo File */
        SAXBuilder constructorSAX = new SAXBuilder();

        try {
            documento = (Document)constructorSAX.build(archivo);
        } catch (JDOMException e) {
            System.out.println("Fichero XML no valido");
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("Fichero no valido");
            e.printStackTrace();
        }
        /* Obtenemos el nodo raiz o principal */
        nodoRaiz = documento.getRootElement();        
        /* Obtenemos la lista de los nodos con la etiqueta*/
        List listaAplicaciones = nodoRaiz.getChildren("paquete");
        /* Recorremos esta lista imprimiendo los elementos
         * dentro de cada aplicacion y su categoría */
        for (int i=0; i<listaAplicaciones.size(); i++) {
            Element nodo = (Element)listaAplicaciones.get(i);
            boolean estado=false;
            resultado=resultado+nodo.getChild("idPaquete").getValue()+","+nodo.getChild("ipOrigen").getValue()+","+nodo.getChild("ipDestino").getValue()+","+nodo.getChild("detalles").getValue()+"/";
        }
        return resultado;
    }
    
    public void agregarInformacionControl (String idPaquete, String ipOrigen, String ipDestino, String detalles)
    {
        /* Creamos una instancia del archivo data.xml */
        archivo = new File(System.getProperty("user.dir")+"//InformacionDeControl.xml");
        /* Creamos el documento xml a partir del archivo File */
        SAXBuilder constructorSAX = new SAXBuilder();
        try {
            documento = (Document)constructorSAX.build(archivo);
        } catch (JDOMException e) {
            System.out.println("Fichero XML no valido");
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("Fichero no valido");
            e.printStackTrace();
        }
            Element staff = new Element("paquete");
            staff.addContent(new Element("idPaquete").setText(idPaquete));
            staff.addContent(new Element("ipOrigen").setText(ipOrigen));
            staff.addContent(new Element("ipDestino").setText(ipDestino));            
            staff.addContent(new Element("detalles").setText(detalles));
            documento.getRootElement().addContent(staff);
            XMLOutputter xmlOutput = new XMLOutputter();

            xmlOutput.setFormat(Format.getPrettyFormat());
        try {
            //System.out.println(new java.io.File(".").getCanonicalPath());
            xmlOutput.output(documento, new FileWriter(System.getProperty("user.dir")+"//InformacionDeControl.xml"));
        } catch (IOException ex) {
            Logger.getLogger(ManejadorXML.class.getName()).log(Level.SEVERE, null, ex);
        }        
}

   
    public void leerInformacionControl () {
        
        /* Creamos una instancia del archivo data.xml */     
        archivo = new File(System.getProperty("user.dir")+"//InformacionDeControl.xml");
        /* Creamos el documento xml a partir del archivo File */
        SAXBuilder constructorSAX = new SAXBuilder();
        try {
            documento = (Document)constructorSAX.build(archivo);
        } catch (JDOMException e) {
            System.out.println("Fichero XML no valido");
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("Fichero no valido");
            e.printStackTrace();
        }        
        /* Obtenemos el nodo raiz o principal */
        nodoRaiz = documento.getRootElement();
        if (nodoRaiz.getName().toString()=="inventario")
        {
            /* Obtenemos la lista de los nodos con la etiqueta*/
            List listaAplicaciones = nodoRaiz.getChildren("producto");
            /* Recorremos esta lista imprimiendo los elementos
             * dentro de cada aplicacion y su categoría */
            for (int i=0; i<listaAplicaciones.size(); i++) {
                /* Obtenemos el elemento de la lista */
                Element nodo = (Element)listaAplicaciones.get(i); 
           //     listaProducto.add(new Producto(nodo.getChild("nombre").getValue().toString(),nodo.getChild("descripcion").getValue().toString(),Integer.parseInt(nodo.getChild("costo").getValue().toString()),nodo.getChild("imagen").getValue().toString(),Integer.parseInt(nodo.getChild("cantidad").getValue().toString()),Boolean.parseBoolean(nodo.getChild("estado").getValue())));
            }        
        }
    }
 
      public String getCharacterDataFromElement(org.w3c.dom.Element e) {
            Node child = e.getFirstChild();
            if (child instanceof CharacterData) {
              CharacterData cd = (CharacterData) child;
              return cd.getData();
            }
            return "";
      }
      
}
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Dominio;

import static Logica.Logica.bitacoraSucursal;
import java.io.Serializable;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author isaacarismendi
 */
public class Sucursal implements Serializable{
    
    private static Sucursal sucursal;
    private String miIp;
    private String ipDestino;
    private List<Paquete> listaEsperaRecepcion;
    private List<Paquete> listaEsperaEnvio;
    private int tiempoEsperaEnLista;
    private int paquetesProcesados;
    
    private Sucursal() {
        InetAddress IP;
        try {
            IP = InetAddress.getLocalHost();
            this.miIp = IP.getHostAddress();
        } catch (UnknownHostException ex) {
            this.miIp = "";
            Logger.getLogger(Sucursal.class.getName()).log(Level.SEVERE, null, ex);
        } 
        this.listaEsperaRecepcion = new ArrayList<>();
        this.listaEsperaEnvio = new ArrayList<>();
        this.tiempoEsperaEnLista = 0;
    }
    
    
    public static Sucursal getInstanceSucursal (){
        if (sucursal==null) {
            sucursal=new Sucursal();
        }        
        return sucursal;
    }
    
    public String getIp() {
        return miIp;
    }

    public void setIp(String miIp) {
        this.miIp = miIp;
    }

    public String getIpDestino() {
        return ipDestino;
    }

    public void setIpDestino(String ipDestino) {
        this.ipDestino = ipDestino;
    }
    
    public int getTiempoEsperaEnLista() {
        return tiempoEsperaEnLista;
    }

    public void setTiempoEsperaEnLista(int tiempoEsperaEnLista) {
        this.tiempoEsperaEnLista = tiempoEsperaEnLista;
    }
    
    public List<Paquete> getListaEsperaRecepcion() {
        return listaEsperaRecepcion;
    }
    
    public List<Paquete> getListaEsperaEnvio() {
        return listaEsperaEnvio;
    }

    public int getPaquetesProcesados() {
        return paquetesProcesados;
    }

    public void setPaquetesProcesados(int paquetesProcesados) {
        this.paquetesProcesados = paquetesProcesados;
    }
    
    
    public void setListaEspetaRecepcion(List<Paquete> listaEspetaRecepcion) {
        this.listaEsperaRecepcion = new ArrayList<>(listaEspetaRecepcion);
    }
    
    public void procesarPaquetes (Paquete paquete) {
        
        Paquete paqueteBorrar = null;
        paquete.getEstadisticas().setTiempoLlegadaDestino(paquete.getEstadisticas().getTiempoLlegadaDestino()+15+this.getTiempoEsperaEnLista());
        paquete.getEstadisticas().setTiempoEspera(this.tiempoEsperaEnLista);
        paqueteBorrar = paquete;
     
        Logica.Logica.transporte.almacenarInformacionSucursal(paquete, "Paquete procesado Exitosamente");

        if (paqueteBorrar != null) { 
            //enviar a central  
            Boolean respuesta = Logica.Logica.enviarBitacoraServidorCentral(paqueteBorrar.getEstadisticas());
            while(respuesta == false){
                respuesta = Logica.Logica.enviarBitacoraServidorCentral(paqueteBorrar.getEstadisticas());
            }
            Logica.Logica.enviarInformacionControl();
            listaEsperaRecepcion.remove(paqueteBorrar);
        }
        this.paquetesProcesados++;
    }
    
    public Boolean addItemListaEsperaRecepcion(Paquete paquete){        
        
        if(this.listaEsperaRecepcion.size() <= 5){
            System.out.println("Recibi paquete de:"+paquete.getIpSucursalOrigen());
            paquete.setTiempoEsperaEnRecepcion(this.tiempoEsperaEnLista);
            this.listaEsperaRecepcion.add(paquete);
            Logica.Logica.transporte.almacenarInformacionSucursal(paquete, "Paquete Recibido Exitosamente");
            return true;
        } 
        return false;
    }

//    public void setListaEspetaEnvio(List<Paquete> listaEspetaEnvio) {
//        this.listaEspetaEnvio = listaEspetaEnvio;
//    }
    
    public Boolean addItemListaEsperaEnvio(Paquete paquete){
        
        if(this.listaEsperaEnvio.size() <= 7 && !paquete.getIpSucursalDestino().equals(this.miIp)){
            this.listaEsperaEnvio.add(paquete);
            System.out.println("Paquete agregado exitosamente a la Lista de Espera para Envio");
            return true;
        }
        //System.out.println("No se pudo agregar a la Lista Espera para Envio");
        
        Logica.Logica.bitacoraSucursal.setNoAceptadoListaEsperaEnvio(1);
        Logica.Logica.enviarBitacoraServidorCentral(bitacoraSucursal);
        Logica.Logica.bitacoraSucursal =  new Bitacora(Logica.Logica.sucursal.getIp(), Logica.Logica.sucursal.getIp());
        System.out.println("No se pudo agregar a la Lista Espera para Envio. Paquete perdido.");   
        return false;
    }
    
}

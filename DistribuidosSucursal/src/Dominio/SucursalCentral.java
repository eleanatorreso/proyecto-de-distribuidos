/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Dominio;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author isaacarismendi
 */
public class SucursalCentral {
    
    private List<String> listaSucursales;
    private List<Bitacora> listaEstadistica;

    public SucursalCentral() {
        this.listaSucursales = new ArrayList<>();
        this.listaEstadistica = new ArrayList<>();
    }

    public List<String> getListaSucursales() {
        return listaSucursales;
    }

    public void setListaSucursales(List<String> listaSucursales) {
        this.listaSucursales = listaSucursales;
    }
    
    public Boolean addListaSucursales(String ip){
        Boolean resultado = true;
        
        for (String elemento : listaSucursales) {
            
            if(elemento.equals(ip)){
                resultado = false;
            }
            else{
                resultado = true;  
            }
            
        }
        
        if(resultado){
            resultado = this.listaSucursales.add(ip);
        }
                
        return resultado;
    }

    public List<Bitacora> getListaEstadistica() {
        return listaEstadistica;
    }

//    public void setListaEstadistica(List<Bitacora> listaEstadistica) {
//        this.listaEstadistica = listaEstadistica;
//    }
    
    public Boolean addItemListaEstadistica(Bitacora bitacora){
        Boolean resultado = false;
        resultado = this.listaEstadistica.add(bitacora);
        return resultado;
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Dominio;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author isaacarismendi
 */
public class Transporte implements Serializable{
    public static Transporte transporte;
    private String informacionControl;
    private List<Paquete> listaPaquetes;
    private int tiempoTrayectoria;
    private int tiempoDeVida;
    private int idPaquete = 0;

    private Transporte() {
        
        this.listaPaquetes = new ArrayList<>();
        this.tiempoTrayectoria = 4; // Tiempo que tarda por defecto
        this.tiempoDeVida = 0;
        this.informacionControl="";
    }
    
    public static Transporte getInstanceTransporte (Transporte transporteRecibido){
        transporte=transporteRecibido;
        return transporte;
    }
    
    public static Transporte getInstanceTransporte (){
        if (transporte==null) {
            transporte=new Transporte();
        }
        return transporte;
    }

    public int getIdPaquete() {
        idPaquete++;
        return idPaquete;
    }

    public void setIdPaquete(int idPaquete) {
        this.idPaquete = idPaquete;
    }

    public int getTiempoDeVida() {
        return tiempoDeVida;
    }

    public void setTiempoDeVida(int tiempoDeVida) {
        this.tiempoDeVida = tiempoDeVida;
    }
    
    public String getInformacionControl() {
        return informacionControl;
    }

    public void setInformacionControl(String informacionControl) {
        this.informacionControl = informacionControl;
    }

    public List<Paquete> getListaPaquetes() {
        return listaPaquetes;
    }
    
    public int getTiempoTrayectoria() {
        return tiempoTrayectoria;
    }

    public void setTiempoTrayectoria(int tiempoTrayectora) {
        this.tiempoTrayectoria = tiempoTrayectora;
    }
    
    public void almacenarInformacionSucursal (Paquete paquete, String detalles) {
        
        informacionControl +=paquete.getId()+","+paquete.getIpSucursalOrigen()+","+paquete.getIpSucursalDestino()+","+detalles+"/";   
    }
    
    public void recalcularTiempoTrayectoria() {
        
        if (listaPaquetes.size()>6) {
            int tiempoAdicional = listaPaquetes.size() - 6;
            tiempoTrayectoria = 4 + tiempoAdicional;
        }
        else if (listaPaquetes.size()<=6) {
            tiempoTrayectoria = 4;
        }
    }
    
    public Boolean addItemListaPaquetes(Paquete paquete) {    
        int tiempoDeRecepcion = 1;
        if(this.listaPaquetes.size() < 10){
            paquete.getEstadisticas().setTiempoLlegadaDestino(tiempoDeRecepcion);
            this.listaPaquetes.add(paquete);
            almacenarInformacionSucursal(paquete, "Paquete Enviado Exitosamente");            
            recalcularTiempoTrayectoria();
            return true;
        }
        System.out.println("Transporte lleno en su totalidad. Paquete No Aceptado.");
        paquete.getEstadisticas().setNoAceptadoTranspote(paquete.getEstadisticas().getNoAceptadoTranspote()+1);
        return false;
    }
    
    public void setearTiempoProcesamientoInformacionControl () {
        for (Paquete paquete : listaPaquetes) {
            paquete.getEstadisticas().setTiempoLlegadaDestino(paquete.getEstadisticas().getTiempoLlegadaDestino()+2);
            
        }
    }
    
    public void setearTiempoTrayecto () {
        for (Paquete paquete : listaPaquetes) {
            paquete.getEstadisticas().setTiempoLlegadaDestino(paquete.getEstadisticas().getTiempoLlegadaDestino()+tiempoTrayectoria);
        }
        tiempoDeVida += tiempoTrayectoria + 1;
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Logica;

import Dominio.Paquete;
import Dominio.Sucursal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author isaacarismendi
 */
public class HiloRecepcionPaquetes implements Runnable{

    public Thread hilo;
    private Sucursal sucursal;
    public List<Paquete> listaPaqueteRecepcion;
    
    public HiloRecepcionPaquetes() {
        sucursal=Sucursal.getInstanceSucursal();
        hilo=new Thread(this);
        hilo.start();
    }
    
    private void procesamiento() {
        Iterator<Paquete> iteradorListPaquete = this.listaPaqueteRecepcion.iterator();
        int contador = 0;
        for (Iterator<Paquete> it = iteradorListPaquete; it.hasNext();) {
            Paquete paquete = it.next();
            if (contador < 3 && paquete.getTiempoEsperaEnRecepcion() == 0) {
                contador++;
                sucursal.procesarPaquetes(paquete);
            }
        }
    }
    
    
    @Override
    public void run() {
       int tiempo = 0;
       Paquete paquete;
       while(true){
           sucursal=Sucursal.getInstanceSucursal();
           this.listaPaqueteRecepcion = new ArrayList<>(sucursal.getListaEsperaRecepcion());
           //listaPaqueteRecepcion = sucursal.getListaEsperaRecepcion();
           Iterator<Paquete> iteradorListPaquete = this.listaPaqueteRecepcion.iterator();
           try {
               for (Iterator<Paquete> it = iteradorListPaquete; it.hasNext();) {
                   paquete =it.next();
                    tiempo = paquete.getTiempoEsperaEnRecepcion();
                    paquete.setTiempoEsperaEnRecepcion(tiempo - 1);
                        
//                    if(paquete.getTiempoEsperaEnRecepcion() <= 0){
//                        sucursal.procesarPaquetes(paquete);
//                    } 
                   
               }
               procesamiento();
               Thread.sleep(1000);
 
            } 
           catch (InterruptedException ex) {
                 Logger.getLogger(HiloRecepcionPaquetes.class.getName()).log(Level.SEVERE, null, ex);
 
           }
               
               
       
       }
    }
    
}

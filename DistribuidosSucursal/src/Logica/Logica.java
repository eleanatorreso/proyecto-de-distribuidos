/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Logica;

import Dominio.Bitacora;
import Dominio.Paquete;
import Dominio.Sucursal;
import Dominio.Transporte;
import static Logica.Logica.actualizarHiloRecepcionPaquetes;
import static Logica.Logica.bajarPaquetesTransporte;
import static Logica.Logica.chequearDisponibilidadTransporte;
import static Logica.Logica.connectServer;
import static Logica.Logica.connectSucursalSiguiente;
import static Logica.Logica.envioTransporte;
import static Logica.Logica.montarPaquetes;
import static Logica.Logica.rmiServidor;
import static Logica.Logica.rmiSucursal;
import static Logica.Logica.tengoTransporte;
import static Logica.Logica.transporte;
import static Logica.Logica.usoTransporte;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.rmi.ConnectException;
import java.rmi.NoSuchObjectException;
import java.rmi.NotBoundException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.UnmarshalException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author isaacarismendi
 */
public class Logica {
    //debo asegurarme que en este punto ya haya instanciado mis datos como sucursal.
    public static Sucursal sucursal;
    public static RMI rmiServidor;
    public static RMISucursal rmiSucursal;
    public static Transporte transporte;
    private static HiloRecepcionPaquetes hiloRecepcionPaquetes;
    public static Boolean tengoTransporte = false;
    private static Boolean enviadoTransporte = false;
    private static Boolean laLocura = false;
    public static Bitacora bitacoraSucursal; 
    private static int primeraVez = 0;
    
    public static Boolean obtenerIpDestino(){
        
        try {
            Boolean resultado = rmiServidor.ConectarmeAlAnillo(sucursal.getIp());
            if(!resultado){
                System.out.println("Su IP esta REPETIDA en el servidor NO se puede conectar en el anillo");
                System.exit(0);
            }
            else{
                connectSucursalSiguiente();
            }
        } catch (RemoteException ex) {
            connectServer(); 
            return false;
        }
        return true;
        
    }
    
    public static void obtenerIpDestino(List<String> listaIp){
        if(listaIp != null){
            for (int i = 0; i < listaIp.size(); i++) {
                try {
                    
                    if(listaIp.get(i).equals(sucursal.getIp())){
                        sucursal.setIpDestino(listaIp.get(i+1));
                        connectSucursalSiguiente();
                        System.out.println("Se conecto a mi lado: "+sucursal.getIpDestino());
                    }
                    
                } catch (Exception e) {
                    sucursal.setIpDestino(listaIp.get(0));  // este caso pasa cuando soy el ultimo de la lista;
                }
            }
        }
        else{
            System.out.println("Ip de la sucursal repetida");
        }
        
    }
    
    
    public static void connectServer(){
        
        List<String> listServer = new ArrayList<>();
        listServer.add("192.168.1.102");
        listServer.add("192.168.1.100");
        listServer.add("192.168.1.103");
        
        for (String ip : listServer) {             
            try {

                Registry reg = LocateRegistry.getRegistry(ip, 8881);
                rmiServidor = (RMI) reg.lookup("Server");// conexion al servidor central
                break;
            } catch (RemoteException | NotBoundException ex) {
                
            } 
        } 
//        if (primeraVez==0)
//            primeraVez = 1;
//        else {
//            System.out.println("Cambio de Servidor");
//            Logica.bitacoraSucursal.setMensajesPerdidosAlServidor(1);
//            Logica.enviarBitacoraServidorCentral(bitacoraSucursal);
//            Logica.bitacoraSucursal =  new Bitacora(Logica.sucursal.getIp(), Logica.sucursal.getIp());
//        }
    }
    
     public static void connectSucursalSiguiente(){
        
         try{
             
            Registry reg = LocateRegistry.getRegistry(sucursal.getIpDestino(), 8882); // conexion al servidor central
            rmiSucursal= (RMISucursal) reg.lookup("Sucursal");
                
        }catch(Exception e){
            
            System.out.println(e);
        }      
         
    }
    
    public static void levantarSucursal(){
        try{
            sucursal= Sucursal.getInstanceSucursal();
            System.setProperty("java.rmi.server.hostname", sucursal.getIp());
            Registry reg = LocateRegistry.createRegistry(8882);
            reg.rebind("Sucursal", (Remote) new ServidorSucursal());
            hiloRecepcionPaquetes = new HiloRecepcionPaquetes();
            bitacoraSucursal = new Bitacora(sucursal.getIp(),sucursal.getIp());
            System.out.println("Sucursal Encendido");
        }   catch(Exception e){
            System.out.println(e);
        }  
    } 
    
    public static void montarPaquetes(){
        boolean resultado;
        for (Iterator<Paquete> it = sucursal.getListaEsperaEnvio().iterator(); it.hasNext();) {
            Paquete paquete = it.next();
            resultado = transporte.addItemListaPaquetes(paquete);   
            if (!resultado) {
                Logica.bitacoraSucursal.setNoAceptadoListaEsperaRecepcion(1);
                Logica.enviarBitacoraServidorCentral(bitacoraSucursal);
                Logica.bitacoraSucursal =  new Bitacora(Logica.sucursal.getIp(), Logica.sucursal.getIp());
            }
        }
    }
    
    public static void bajarPaquetesTransporte () {
        List<Paquete> listaPaquete = transporte.getInstanceTransporte().getListaPaquetes();
        Boolean resultado = false;
        //no se si esto va a aca
       //transporte.setearTiempoProcesamientoInformacionControl();
        List<Paquete> listaAuxiliar= new ArrayList<Paquete>();
        for (Paquete paquete : listaPaquete) {
            paquete.getEstadisticas().setTiempoLlegadaDestino(transporte.getTiempoTrayectoria());
            if (paquete.getIpSucursalDestino().equals(sucursal.getIp())) {
                resultado = sucursal.addItemListaEsperaRecepcion(paquete);
                if (resultado==true) {
                    paquete.setTiempoEsperaEnRecepcion(sucursal.getTiempoEsperaEnLista());
                    listaAuxiliar.add(paquete);
                }
                else {
                    System.out.println("Lista de recepción FULL, no pudo ser aceptado por el transporte.");
                    
                    paquete.getEstadisticas().setNoAceptadoSucursal(paquete.getEstadisticas().getNoAceptadoSucursal()+1);
                }
            }
        }
        if (listaAuxiliar!=null) {
            transporte.getListaPaquetes().removeAll(listaAuxiliar);
            transporte.recalcularTiempoTrayectoria();
        }
    }

    
    public static Boolean chequearDisponibilidadTransporte () {
        
        if (transporte.getListaPaquetes().size()==10) {
            try {
                rmiServidor.enviarEstadoTransporte(transporte.getTiempoTrayectoria(), transporte.getTiempoDeVida());
                
            } catch (RemoteException ex) {
                connectServer();
                return false;
            }
        }
        return true;
        
    }
    
    private static void generarPaquetes(){
        String pc1 = "192.168.1.114";
        String pc2 = "192.168.1.100";
        String pc3 = "192.168.1.102";
        Paquete paquete;
        
        paquete = new Paquete(transporte.getIdPaquete(),"Voy a la pc 1 desde la pc 2",pc1);
        sucursal.addItemListaEsperaEnvio(paquete);
        sucursal.addItemListaEsperaEnvio(paquete);
        sucursal.addItemListaEsperaEnvio(paquete);
        paquete = new Paquete(transporte.getIdPaquete(),"Voy a la pc 1 desde la pc 4",pc2);
        sucursal.addItemListaEsperaEnvio(paquete);
        sucursal.addItemListaEsperaEnvio(paquete);
        sucursal.addItemListaEsperaEnvio(paquete);
        paquete = new Paquete(transporte.getIdPaquete(),"Voy a la pc 1 desde la pc 4",pc3);
        sucursal.addItemListaEsperaEnvio(paquete);
        sucursal.addItemListaEsperaEnvio(paquete);
        sucursal.addItemListaEsperaEnvio(paquete);
        sucursal.addItemListaEsperaEnvio(paquete);
        sucursal.addItemListaEsperaEnvio(paquete);
        sucursal.addItemListaEsperaEnvio(paquete);
    }
    
    public static void usoTransporte (Transporte trans) {
        Boolean resultado;
        transporte = transporte.getInstanceTransporte(trans);
        generarPaquetes();
        transporte.setearTiempoTrayecto();
        resultado = chequearDisponibilidadTransporte();
        while(resultado == false){
            resultado = chequearDisponibilidadTransporte();
        }
        bajarPaquetesTransporte();
        montarPaquetes();
        boolean a = true;
        boolean b = true;
        while (a) {
            try {
                Thread.sleep(5000);
            } catch (InterruptedException ex) {
                Logger.getLogger(Logica.class.getName()).log(Level.SEVERE, null, ex);
            }
            if(!Sucursal.getInstanceSucursal().getIpDestino().equals(Sucursal.getInstanceSucursal().getIp())){                
                try {   
                        System.out.println("Presione ENTER para enviar transporte");
                        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                        br.readLine();      
                        while (b == true){ 
                            try {
                                rmiServidor.notificarEnvioTransporte(sucursal.getIp());
                                b = false;
                            } catch (ConnectException | NoSuchObjectException ex) {
                                connectServer();
                            }
                        }
                        System.out.println("Transporte Enviado");
                        tengoTransporte = false;
                        envioTransporte();
                        if(!laLocura){
                            while(enviadoTransporte == false){

                                envioTransporte();
                            }
                            if(enviadoTransporte == true){
                                a = false;
                            }
                        }
                        else{
                             a = false;
                        }
                } catch (RemoteException ex) {
                    Logger.getLogger(Logica.class.getName()).log(Level.SEVERE, null, ex);
                    
                } catch (IOException ex) {
                    Logger.getLogger(Logica.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            
        }
    }
    
    
    
    public static Boolean setTransporte (Transporte trans) {
        tengoTransporte = true;
        if (!Sucursal.getInstanceSucursal().getIpDestino().equals(Sucursal.getInstanceSucursal().getIp())) {
            actualizarHiloRecepcionPaquetes();
            usoTransporte(trans);
            
            return true;
        }
        return false;
    }
    
     public static void envioTransporte () throws RemoteException{
        
         try {
            enviadoTransporte = true;
            
            rmiSucursal.enviarTransporte(transporte);        
        }
        
        catch(ConnectException ex){
            enviadoTransporte = false;            
            bitacoraSucursal.setSucursalCaida(1);
            enviarBitacoraServidorCentral(bitacoraSucursal);         
            Logica.bitacoraSucursal =  new Bitacora(Logica.sucursal.getIp(), Logica.sucursal.getIp());
            System.out.println("Sucursal Destino Caida");            
            rmiServidor.reconstruirAnillo();  
            connectSucursalSiguiente();
            
        }catch(UnmarshalException ex2){
            laLocura = true;            
        }
    }
    
    public static Boolean newTransporte () { 
        tengoTransporte = true;
        if(transporte == null){
           transporte=transporte.getInstanceTransporte();
           usoTransporte(transporte); 
        }   
        return true;                    
    }
    
    public static void enviarInformacionControl () {
        try {
            rmiServidor.recibirInformacionControl(transporte.getInformacionControl());
            transporte.setInformacionControl("");
        } catch (RemoteException ex) {
            System.out.println(ex);
        }
    }
    
    public static Boolean enviarBitacoraServidorCentral(Bitacora bitacora){
        try {
            return rmiServidor.recibirBitacora(bitacora);
        } catch (RemoteException ex) {

            connectServer();
            return false;
        }
    }
    
    public static void actualizarHiloRecepcionPaquetes(){
        hiloRecepcionPaquetes.hilo.stop();
        
        if(hiloRecepcionPaquetes.listaPaqueteRecepcion != null && hiloRecepcionPaquetes.listaPaqueteRecepcion.size() > 0){           
            sucursal.setListaEspetaRecepcion(hiloRecepcionPaquetes.listaPaqueteRecepcion);        
        }
        //hiloRecepcionPaquetes.hilo.start(); 
        hiloRecepcionPaquetes = new HiloRecepcionPaquetes();
    }
    
}


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Logica;

import Dominio.Bitacora;
import Dominio.Transporte;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.List;

/**
 *
 * @author isaacarismendi
 */
public class ServidorSucursal extends UnicastRemoteObject implements RMISucursal{

    public ServidorSucursal() throws RemoteException {
        super();
    }

  
    @Override
    public void actulizarAnillo(List<String> listaIp) throws RemoteException {
        Logica.obtenerIpDestino(listaIp);
    }

    @Override
    public void enviarTransporte(Transporte transporte) throws RemoteException {
         Logica.setTransporte(transporte);
    }

    @Override
    public Boolean crearTransporte() throws RemoteException {
        return Logica.newTransporte();
    }

    @Override
    public Boolean reenviarTransporte() throws RemoteException {
       
        try {
             System.out.println("Reconstruyendo el transporte");
             Logica.bitacoraSucursal.setDesaparicionDelTransporte(1);
             Logica.enviarBitacoraServidorCentral(Logica.bitacoraSucursal);
             Logica.bitacoraSucursal =  new Bitacora(Logica.sucursal.getIp(), Logica.sucursal.getIp());          
             Logica.rmiServidor.reconstruirAnillo();
             Logica.envioTransporte();
             return true;
        } catch (Exception e) {
            
        }
        return false;
    }

    @Override
    public Boolean tienesElTransporte() throws RemoteException {
        return Logica.tengoTransporte;
    }
    
    
}

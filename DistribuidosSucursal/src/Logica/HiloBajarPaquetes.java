/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Logica;

import Dominio.Paquete;
import Dominio.Sucursal;
import Dominio.Transporte;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Eleana
 */
public class HiloBajarPaquetes implements Runnable{
    private static Transporte transporte;
    private static Sucursal sucursal;
    private Thread hilo;
    
    public HiloBajarPaquetes() {
        transporte=Transporte.getInstanceTransporte();
        sucursal=Sucursal.getInstanceSucursal();
        hilo=new Thread(this);
        hilo.start();
    }
    
    public static void bajarPaquetesTransporte () {
        List<Paquete> listaPaquete = transporte.getInstanceTransporte().getListaPaquetes();
        Boolean resultado = false;
        int size = listaPaquete.size();
        List<Paquete> listaAuxiliar = new ArrayList<Paquete>();
        for (Paquete paquete : listaPaquete) {
            paquete.getEstadisticas().setTiempoLlegadaDestino(transporte.getTiempoTrayectoria());
            if (paquete.getIpSucursalDestino().equals(sucursal.getIp())) {
                resultado = sucursal.addItemListaEsperaRecepcion(paquete);
                if (resultado == true) {
                    listaAuxiliar.add(paquete);
                }
                else {
                    System.out.println("Lista de recepción FULL");
                    paquete.getEstadisticas().setNoAceptadoSucursal(paquete.getEstadisticas().getNoAceptadoSucursal()+1);
                }
            }
        }
        if (listaAuxiliar != null) {
            transporte.getListaPaquetes().removeAll(listaAuxiliar);
        }
    }
    
    @Override
    public void run() {
        bajarPaquetesTransporte();
    }
    
    
}

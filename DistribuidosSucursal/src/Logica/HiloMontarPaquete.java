/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Logica;

import Dominio.Paquete;
import Dominio.Sucursal;
import Dominio.Transporte;

/**
 *
 * @author Eleana
 */
public class HiloMontarPaquete implements Runnable{
    private static Transporte transporte;
    private static Sucursal sucursal;
    private Paquete paquete;
    private Thread hilo;
    
    public HiloMontarPaquete(Paquete paquete) {
        transporte=Transporte.getInstanceTransporte();
        sucursal=Sucursal.getInstanceSucursal();
        hilo=new Thread(this);
        hilo.start();
        this.paquete=paquete;
    }
    
     private void montarPaquetes(){
        boolean resultado;
        resultado = transporte.addItemListaPaquetes(paquete);        
        if(!resultado){
            sucursal.addItemListaEsperaEnvio(paquete);
        }
        
    }
    
    @Override
    public void run() {
        montarPaquetes();
    }
    
    
}

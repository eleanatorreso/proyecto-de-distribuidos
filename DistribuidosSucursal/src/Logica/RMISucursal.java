/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Logica;

import Dominio.Transporte;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

/**
 *
 * @author isaacarismendi
 */
interface RMISucursal  extends Remote{
    public void enviarTransporte(Transporte transporte) throws RemoteException;
    public void actulizarAnillo(List<String> listaIp) throws RemoteException;
    public Boolean crearTransporte() throws RemoteException;
    public Boolean reenviarTransporte() throws RemoteException;
    public Boolean tienesElTransporte () throws RemoteException;
}
